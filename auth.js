//this file will contain the logic for authorization via token and create function to create access token
//jwt-json web token
const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//jwt.sign(payload, secretKey, option)
	return jwt.sign(data, secret, {});

}

//add logic to verify and decode the user info
//next passes on the request to the next middleware function/route/request handler in the stack
module.exports.verify = (req, res, next) => {
	let token = req.header.authorization
	if (typeof token !== 'undefined') {
		//used to get the token only
		//removes the "bearer" from the received token
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			//next() passes the req to the next cllback func in the route
			//next callback func = (req, res) => {}
			return (err) ? res.send ({ auth: 'failed' }) : next()
		})
	} else {
		return res.send({ auth: 'failed' })
	}
}

module.exports.decode = () => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, { complete: true}).payload
		})
	} else {
		return null
	}
}