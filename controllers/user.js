const User = require('../model/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Course = require('../model/Course')

//all logic created in controllers
module.exports.emailExists =  (params) => {
	return User.find( { email: params.email })
	.then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10) //method that provide to encripted pw: 10 by default
	})
	return user.save().then((user, err) => {
		return (err) ? true : false
	})
}

//require the authentication logic for logging in a user
module.exports.login = (params) => {
	return User.findOne({ email: params.email })
	.then(user => {
		if (user == null) {
			return false
		}
		//compare pw received & hashed pw
		//return true if values match
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		//mongoose toObject methods converts the mongoose into plain js object
		//used to show an object representation of mongoose mode
		//mongoose object will have access to .save() method while plain js want
		if (isPasswordMatched){
			return { accessToken: auth.createAccessToken(
				user.toObject())
			}
		} else {
			return false
		}
		console.log(user);
		console.log(user.toObject());
	})
}

//create the controller logic for getting the user info

module.exports.get = () => {
	return User.findById(params.userId).then(user => {
		//reassign the pw to undefined so it won't be retrieved along with other user data
		user.password = undefined
		return user
	})
}
/*
module.exports.register = (params) => {
	let course = new course({
		name: params.name,
		description: params.description,
		price: params.price,
		isActive: params.isActive,
		createdOn: params.createdOn
		 //method that provide to encripted pw: 10 by default
	})
	return user.save().then((user, err) => {
		return (err) ? true : false
	})
}
*/
module.exports.enroll = (params) => {
	return User.findById(params.userId).then ( user => {
		user.enrollments.push({ courseId: params.courseId })

	return user.save().then((user,err) => {
		return Course.findById(params.courseId)
		.then (course => {
			course.enrollees.push({ userId: params.userId })

		return course.save().then((course, err) => {
			return (err) ? false : true
				})

			})
		})
	})
}