const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
//require and configure .env
require('dotenv').config()

//connect to mongoDB
const connectionString = process.env.DB_CONNECTION_STRING
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/users', userRoutes)


//process.env - para mahide yung mga process na gustong ihide
const port = process.env.PORT;
app.listen(port, () => console.log(`You got served on port ${port}!`))
